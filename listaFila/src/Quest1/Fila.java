package Quest1;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Fila {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		Queue<Playlist> fila = new LinkedList<>();
		operacoes(fila);
	}

	public static void operacoes(Queue<Playlist> fila) {

		Integer op = 0;
		do {
			System.out.println(
					"Informe qual operao deseja fazer: 1=inserir musica, 2=consultar musica, 3=remover musica, 4=sair");
			op = sc.nextInt();
			sc.nextLine();
			switch (op) {
			case 1: {
				fila.add(addMusica());
				System.out.println("Musica adicionada com sucesso.");
				break;
			}
			case 2: {
				for (Playlist playlist : fila) {
					System.out.println(playlist.toString());
				}
				System.out.println("\n");

				break;
			}
			case 3: {
				try {
					fila.remove();
					System.out.println("A primeira m�sica da playlist foi exclu�da.");
				} catch (Exception e) {
					System.out.println("A m�sica n�o existe na playlist");
				}
				break;
			}
			case 4: {
				System.out.println("Voc� saiu da playlist.");
				System.exit(0);

				break;
			}
			default:
			}
		} while (true);
	}

	public static Playlist addMusica() {
		System.out.print("Informe o nome da musica: ");
		String nome = sc.nextLine();

		return new Playlist(nome);
	}

}
