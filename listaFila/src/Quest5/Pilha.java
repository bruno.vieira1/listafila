package Quest5;

import java.util.Scanner;
import java.util.Stack;

public class Pilha {
	static Scanner sc = new Scanner(System.in);
	static Stack<Paciente> pilha = new Stack<Paciente>();

	public static void main(String[] args) {

		operacoes(pilha);
	}

	public static void operacoes(Stack<Paciente> pilha) {
		int op = 0;
		do {
			System.out.println("Informe as opera��es que deseja realizar: ");
			System.out.println("1=Inserir Paciente," + " 2=Atender paciente," + " 3=Verificar se h� paciente,"
					+ " 4=Indicar o pr�ximo paciente a ser atendido,"
					+ " 5=Quantidade de pacientes que aguardam atendimento, 6=Sair ");
			op = sc.nextInt();
			sc.nextLine();
			switch (op) {
			case 1:
				pilha.push(addPaciente());
				System.out.println("Paciente adicionado.");
				break;
			case 2:
				// N�o consegui pensar em uma l�gica para atender um paciente utilizando pilhas.
				break;
			case 3:
				if (pilha.size() > 0) {
					System.out.println("H� pacientes!");
				} else {
					System.out.println("Sem pacientes no momento.");
				}

				break;
			case 4:
				System.out.println(pilha.peek() + " � o pr�ximo a para ser atendimento");
				break;
			case 5:
				System.out.println("existem " + pilha.size() + " na fila.");

				break;
			case 6:
				System.out.println("Obrigado por utilizar nossos servi�os!");
				System.exit(0);
				break;

			default:
				System.out.println("Voc� informou um n�mero inv�lido para qualquer opera��o.");
				break;
			}
		} while (true);
	}

	public static Paciente addPaciente() {
		System.out.println("Informe o nome do Paciente: ");
		String nome = sc.nextLine();

		return new Paciente(nome);

	}

}
