package Quest3;

public class FilaCliente {
	String nome;

	public FilaCliente() {

	}

	public FilaCliente(String pessoa) {
		nome = pessoa;
	}

	public String getPessoa() {
		return nome;
	}

	public void setPessoa(String pessoa) {
		nome = pessoa;
	}

	@Override
	public String toString() {
		return "Cliente [pessoa=" + nome + "]";
	}
}
