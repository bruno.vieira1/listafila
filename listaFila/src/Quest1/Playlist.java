package Quest1;

public class Playlist {
	private String nome;

	public Playlist() {

	}

	public Playlist(String nome) {
		super();
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "Playlist [nome=" + nome + "]";
	}
}
