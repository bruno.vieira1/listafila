package Quest4;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;

public class Fila {
	public static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		Queue<Alunos> fila = new LinkedList<Alunos>();
		operacoes(fila);

	}

	public static void operacoes(Queue<Alunos> fila) {
		Integer op = 0;
		do {
			System.out.println("Informe a opera��o que deseja realizar:");
			System.out.println("1=inserir alunos // 2=consultar alunos // 3=sair.");
			op = sc.nextInt();
			sc.nextLine();

			switch (op) {
			case 1: {
				fila.add(addAlunos());
				System.out.println("Aluno adicionado na fila.");
				break;
			}
			case 2: {
				((List<Alunos>) fila).sort((a, b) -> Integer.compare(a.getIdade(), b.getIdade()));
				for (Alunos alunos : fila) {
					System.out.println(alunos.toString());
				}

				break;
			}
			case 3: {
				System.out.println("Obrigado por utilizar nossos servi�os :)");
				System.exit(0);

				break;
			}
			default:
			}

		} while (true);
	}

	public static Alunos addAlunos() {
		System.out.println("Informe o nome do aluno: ");
		String nome = sc.nextLine();
		System.out.println("Informe a idade do aluno: ");
		Integer idade = sc.nextInt();

		return new Alunos(nome, idade);

	}

}
