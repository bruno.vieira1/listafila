package Quest3;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Fila {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		Queue<FilaCliente> fila = new LinkedList<>();

		Operacoes(fila);
	}

	public static void Operacoes(Queue<FilaCliente> fila) {
		Integer op = 0;
		do {
			System.out.println("Informe a opera��o que deseja realizar:"
					+ "\n1=Inserir cliente:\n2=Remover cliente:\n3=Consultar quantos clientes tem na fila:\n4=Sair.");
			op = sc.nextInt();
			sc.nextLine();

			switch (op) {
			case 1: {
				fila.add(addCliente());
				System.out.println("Cliente adicionado na fila.");
				break;
			}
			case 2: {
				fila.remove();
				System.out.println("O primeiro cliente da fila foi dispensado.");
				break;
			}
			case 3: {
				System.out.println(fila.size());
				break;
			}
			case 4: {
				System.out.println("execuss�o encerrada.");
				System.exit(0);
				break;
			}
			default:
			}
		} while (true);
	}

	public static FilaCliente addCliente() {
		System.out.println("Informe o nome do cliente:");
		String nome = sc.nextLine();
		return new FilaCliente(nome);

	}

}
